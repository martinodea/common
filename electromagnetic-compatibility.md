## ELECTROMAGNETIC COMPATIBILITY

European Council Directive 89/336/EEC requires electronic equipment not to generate electromagnetic disturbances exceeding defined levels and have adequate immunity levels for normal operation. Specific standards applicable to this analyser are stated below.

As there are electrical products in use pre-dating this Directive, they may emit excess electromagnetic radiation levels and, occasionally, it may be appropriate to check the analyser before use by:

- Use the normal start up sequence in the location where the analyser will be used.

- Switch on all localized electrical equipment capable of causing interference.

- Check all readings are as expected. A level of disturbance is acceptable.

- If not acceptable, adjust the analyser’s position to minimize interference or switch off, if possible, the offending equipment during your test.

At the time of writing this manual (January 2017) Kane International Ltd are not aware of any field based situation where such interference has occurred and this advice is only given to satisfy the requirements of the Directive.

This product has been tested for compliance with the following generic standards:

-  EN 61000-6-3 : 2011
-  EN 61000-6-1 : 2007

and is certified to be compliant

Specification EC/EMC/KI/KANE458 details the specific test configuration, performance and conditions of use.

![CE mark](https://s3-eu-west-1.amazonaws.com/kane-uk/images/ce.png)
